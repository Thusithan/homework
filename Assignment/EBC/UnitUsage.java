package EBCSystem;

public class UnitUsage {
	public int id;
	public String month;
	public String year;
	public int unitCount;
	
	public void setUnitUsageDetails(String month,String year, int unitCount) {
		
		this.month = month;
		this.year = year;
		this.unitCount = unitCount;
	}
	
	public int getTotalPriceForUnit () {
		if(this.unitCount<90) {
			return this.unitCount*10;
		}else if (this.unitCount >= 90 && this.unitCount<=120) {
			return this.unitCount*15;
		}else {
			return this.unitCount*20;

		}
	}
}
