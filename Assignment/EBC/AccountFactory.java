package EBCSystem;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class AccountFactory {
	public void insertAccount(Account account) throws SQLException, Exception {
		Connection con = DBConnection.getConnection();
		String query ="INSERT INTO ebc.account (accountNumber, firsName, lastName) VALUES (?, ?, ?)";
		PreparedStatement st = con.prepareStatement(query);
		
		st.setString(1, account.accountNumber);
		st.setString(2, account.firsName);
		st.setString(3, account.lastName);
		st.executeUpdate();
	}
	public List<Account> listAccounts() throws SQLException, Exception {
		Connection con = DBConnection.getConnection();
		String query ="select * from ebc.account";
		List<Account> accounts = new ArrayList<Account>();
		// create the java statement
	      Statement st = con.createStatement();
	      
	      // execute the query, and get a java resultset
	      ResultSet rs = st.executeQuery(query);
	      
	      // iterate through the java resultset
	      while (rs.next())
	      {
	        Account acc = new Account();
	        acc.id = rs.getInt("id");
	        acc.accountNumber = rs.getString("accountNumber");
	        acc.firsName = rs.getString("firsName");
	        acc.lastName = rs.getString("lastName");
	        accounts.add(acc);
	      }
	      st.close();
	      return accounts;
	}
}
