package EBCSystem;

import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Menu {
	private static Admin adminObj = new Admin();
	private static AccountFactory accountFactory = new AccountFactory();
	private static UnitUsageFactory unitUsageFactory = new UnitUsageFactory();
	private static Scanner scannerObj = new Scanner(System.in); // Create a Scanner object

	public static void main(String args[]) throws SQLException, Exception // static method
	{
		System.out.println("Welcome To Electricity bill calculation system");
		Login();
	}
	
	public static void Login() throws SQLException, Exception  {
			boolean isSucess = adminObj.Login();
			if (isSucess) {
				showMenu();
			} else {
				Login();
			}
	}

	public static void showMenu() throws SQLException, Exception {

		adminObj.Menu();
		int input = scannerObj.nextInt(); // Read user input
		while (input > 6) {
			System.out.println("Enter again the Correct id");
			input = scannerObj.nextInt();
		}
		boolean doAgain = true;
		String YesorNo = "y";
		switch (input) {
		case 1:
			while (doAgain) {
				insertAccounts();
				System.out.println("Do you want to Insert more account details ?");
				System.out.println("Press y for yes");
				System.out.println("Press n for No");
				YesorNo = scannerObj.nextLine();
				while (!(YesorNo.equals("y") || YesorNo.equals("n"))) {
					System.out.println("!!!!!! Please press the correct option !!!!!!!");
					YesorNo = scannerObj.nextLine();
				}
				doAgain = !YesorNo.equals("n");
			}
			showMenu();
			break;
		case 2:
			listAccounts(true);
			showMenu();
			break;
		case 3:
			List<Account> currentAccounts = listAccounts(false);
			String accountNumber = "";
			if (currentAccounts.size() == 0) {
				doAgain = false;
				System.out.println("No accounts found, please insert it first");
			} else {
				System.out.println("Please Enter the Account Number");
				while (accountNumber.isEmpty()) {
					accountNumber = scannerObj.nextLine();
				}
			}
			while (doAgain) {
				doAgain = insertMonthlyUsage(currentAccounts, accountNumber);
				if (doAgain) {
					System.out.println("Do you want to insert more usage details ?");
					System.out.println("Press y for yes");
					System.out.println("Press n for No");
					YesorNo = scannerObj.nextLine();
					while (!(YesorNo.equals("y") || YesorNo.equals("n"))) {
						System.out.println("!!!!!! Please press the correct option !!!!!!!");
						YesorNo = scannerObj.nextLine();
					}
					doAgain = !YesorNo.equals("n");
				}
			}
			showMenu();
			break;

		case 4:
			List<Account> allAccounts = listAccounts(false);
			String searchKey = "";
			if (allAccounts.size() == 0) {
				doAgain = false;
				System.out.println("No accounts found, please insert it first");
			}
			while (doAgain) {
				System.out.println("Please Enter the first name or Last Name");
				while (searchKey.isEmpty()) {
					searchKey = scannerObj.nextLine();
				}
				SearchAccount(allAccounts, searchKey);
				searchKey="";
				System.out.println("Do you want to Search again ?");
				System.out.println("Press y for yes");
				System.out.println("Press n for No");
				YesorNo = scannerObj.nextLine();
				while (!(YesorNo.equals("y") || YesorNo.equals("n"))) {
					System.out.println("!!!!!! Please press the correct option !!!!!!!");
					YesorNo = scannerObj.nextLine();
				}
				doAgain = !YesorNo.equals("n");

			}
			showMenu();
			break;
		case 5:
			currentAccounts = listAccounts(false);
			accountNumber="";
			if (currentAccounts.size() == 0) {
				doAgain = false;
				System.out.println("No accounts found, please insert it first");
			}
			while (doAgain) {
				System.out.println("Please Enter the Account Number");
				while (accountNumber.isEmpty()) {
					accountNumber = scannerObj.nextLine();
				}
				findyearBillAndAverage(currentAccounts, accountNumber);
				accountNumber="";
				System.out.println("Do you want to try another account ?");
				System.out.println("Press y for yes");
				System.out.println("Press n for No");
				YesorNo = scannerObj.nextLine();
				while (!(YesorNo.equals("y") || YesorNo.equals("n"))) {
					System.out.println("!!!!!! Please press the correct option !!!!!!!");
					YesorNo = scannerObj.nextLine();
				}
				doAgain = !YesorNo.equals("n");

			}
			showMenu();
			break;

		case 6:
			accountNumber="";
			currentAccounts = listAccounts(false);
			if (currentAccounts.size() == 0) {
				doAgain = false;
				System.out.println("No accounts found, please insert it first");
			}
			while (doAgain) {
				System.out.println("Please Enter the Account Number");
				while (accountNumber.isEmpty()) {
					accountNumber = scannerObj.nextLine();
				}
				findBill(currentAccounts, accountNumber);
				accountNumber="";
				System.out.println("Do you want to view another Bill ?");
				System.out.println("Press y for yes");
				System.out.println("Press n for No");
				YesorNo = scannerObj.nextLine();
				while (!(YesorNo.equals("y") || YesorNo.equals("n"))) {
					System.out.println("!!!!!! Please press the correct option !!!!!!!");
					YesorNo = scannerObj.nextLine();
				}
				doAgain = !YesorNo.equals("n");
			}
			showMenu();
			break;

		default:
			System.out.println("Wrong input, Please try again");
			adminObj.Menu();
		}

	}

	public static void findBill(List<Account> accounts, String accountNumber) throws SQLException, Exception {
		Account selectedAccount = accounts.stream().filter(account -> accountNumber.equals(account.accountNumber))
				.findAny().orElse(null);
		if (selectedAccount != null) {

			System.out.println("please enter Month in number format Eg - 01,02,03");
			int month = scannerObj.nextInt();
			while (month < 0 || month > 12) {
				System.out.println("please enter Correct Month in number format Eg - 01,02,03");
				month = scannerObj.nextInt();
			}

			System.out.println("please enter year in number format Eg - 2020,2021,2022");
			int year = scannerObj.nextInt();
			while (year < 0 || year > 9999) {
				System.out.println("please enter Correct year in number format Eg - 2020,2021,2022");
				year = scannerObj.nextInt();
			}
			List<UnitUsage> unitUsages = listUsageByAccount(selectedAccount.id);
			String monthvalue = String.valueOf(month);
			String yearValue = String.valueOf(year);
			UnitUsage selectedUnitUsage = unitUsages.stream()
					.filter(unitUsage -> monthvalue.equals(unitUsage.month) && yearValue.equals(unitUsage.year))
					.findAny().orElse(null);

			if (selectedUnitUsage != null) {
				System.out.println("Account Number - " + accountNumber);
				System.out
						.println("Account Holder Name  - " + selectedAccount.firsName + " " + selectedAccount.lastName);
				System.out.println("year - " + selectedUnitUsage.year);
				System.out.println("Month - " + selectedUnitUsage.month);
				System.out.println("Unit - " + selectedUnitUsage.unitCount);
				System.out.println("Month Bill Amount - " + selectedUnitUsage.getTotalPriceForUnit());
			} else {
				System.out.println("Unalble to find the record for the Enterd Year and month!!");
			}

		} else {
			System.out.println("Unalble to find the account Holder Please try again !!");
		}

	}

	public static void findyearBillAndAverage(List<Account> accounts, String accountNumber)
			throws SQLException, Exception {
		Account selectedAccount = accounts.stream().filter(account -> accountNumber.equals(account.accountNumber))
				.findAny().orElse(null);
		if (selectedAccount != null) {
			System.out.println("please enter year in number format Eg - 2020,2021,2022");
			int year = scannerObj.nextInt();
			while (year < 0 || year > 9999) {
				System.out.println("please enter Correct year in number format Eg - 2020,2021,2022");
				year = scannerObj.nextInt();
			}
			String yearValue = String.valueOf(year);
			List<UnitUsage> unitUsages = listUsageByAccount(selectedAccount.id);
			if (unitUsages != null && unitUsages.size() > 0) {
				List<UnitUsage> searchResult = unitUsages.stream().filter(unitUsage -> yearValue.equals(unitUsage.year))
						.collect(Collectors.toList());
				if (searchResult != null && searchResult.size() > 0) {
					int totalYearBill = 0;
					for (final UnitUsage uu : searchResult) {
						totalYearBill += uu.getTotalPriceForUnit();
					}
					System.out.println("Account Number - " + accountNumber);
					System.out.println("Selected Year - " + yearValue);
					System.out.println("Total bill amount for the year - " + totalYearBill);
					System.out.println("Average bill amount per month - " + (totalYearBill / searchResult.size()));

				} else {
					System.out.println("Unalble to find the record for the Enterd Year !!");
				}
			} else {
				System.out.println("Unalble to find the record for the account holder try again !!");
			}

		} else {
			System.out.println("Unalble to find the account Holder Please try again !!");
		}

	}

	public static void insertAccounts() throws SQLException, Exception {

		System.out.println("please enter First Name");
		String firsName = "";
		while (firsName.isEmpty()) {
			firsName = scannerObj.nextLine();
		}

		System.out.println("please enter last Name");
		String LastName = scannerObj.nextLine();

		System.out.println("please enter Account number");
		String accountNumber = scannerObj.nextLine();

		Account accountObj = new Account();
		accountObj.setAccountDetails(accountNumber, firsName, LastName);
		accountFactory.insertAccount(accountObj);
	}

	public static List<Account> listAccounts(boolean isprint) throws SQLException, Exception {
		List<Account> accounts = accountFactory.listAccounts();
		if (isprint) {
			if (accounts.size() > 0) {
				System.out.println("AC No\tFirst Name\tLast Name");
				for (final Account acc : accounts) {
					acc.PrintAccount();
				}
			} else {
				System.out.println("No records found");
			}
		}
		return accounts;
	}

	public static void SearchAccount(List<Account> accounts, String searchKey) {
		List<Account> searchResult = accounts.stream()
				.filter(account -> searchKey.equals(account.firsName) || searchKey.equals(account.lastName))
				.collect(Collectors.toList());
		if (searchResult != null && searchResult.size() > 0) {
			System.out.println("AC NO\tFirst Name\tLast Name");
			for (final Account acc : searchResult) {
				acc.PrintAccount();
			}
		} else {
			System.out.println("No records found");
		}

	}

	public static boolean insertMonthlyUsage(List<Account> accounts, String accountNumber)
			throws SQLException, Exception {

		Account selectedAccount = accounts.stream().filter(account -> accountNumber.equals(account.accountNumber))
				.findAny().orElse(null);
		if (selectedAccount != null) {
			System.out.println("Insert the details of Account Holder " + selectedAccount.firsName + " "
					+ selectedAccount.lastName);

			System.out.println("please enter year in number format Eg - 2020,2021,2022");
			int year = scannerObj.nextInt();
			while (year < 0 || year > 9999) {
				System.out.println("please enter Correct year in number format Eg - 2020,2021,2022");
				year = scannerObj.nextInt();
			}
			System.out.println("please enter Month in number format Eg - 01,02,03");
			int month = scannerObj.nextInt();
			while (month < 0 || month > 12) {
				System.out.println("please enter Correct Month in number format Eg - 01,02,03");
				month = scannerObj.nextInt();
			}

			
			List<UnitUsage> unitUsages = listUsageByAccount(selectedAccount.id);
			String monthvalue = String.valueOf(month);
			String yearValue = String.valueOf(year);
			UnitUsage selectedUnitUsage = unitUsages.stream()
					.filter(unitUsage -> monthvalue.equals(unitUsage.month) && yearValue.equals(unitUsage.year))
					.findAny().orElse(null);
			if (selectedUnitUsage == null) {
				System.out.println("please enter Cosumed unit");
				int unit = scannerObj.nextInt();
				while (unit < 0) {
					System.out.println("please enter Correct Cosumed unit count");
					unit = scannerObj.nextInt();
				}

				UnitUsage unitUsageObj = new UnitUsage();
				unitUsageObj.setUnitUsageDetails(monthvalue, yearValue, unit);
				unitUsageFactory.insertUnitUsage(unitUsageObj, selectedAccount.id);
				System.out.println("Record Inserted successfully");
				System.out.println("Account Number - " + accountNumber);
				System.out.println("Month - " + unitUsageObj.month);
				System.out.println("year - " + unitUsageObj.year);
				System.out.println("Unit - " + unitUsageObj.unitCount);
				System.out.println("Monthly Bill Amount - " + unitUsageObj.getTotalPriceForUnit());

			} else {
				System.out.println("Record already inserted - Details below");
				System.out.println("Account Number - " + accountNumber);
				System.out.println("Month - " + selectedUnitUsage.month);
				System.out.println("year - " + selectedUnitUsage.year);
				System.out.println("Unit - " + selectedUnitUsage.unitCount);
				System.out.println("Month Bill Amount - " + selectedUnitUsage.getTotalPriceForUnit());

			}
			return true;
		} else {
			System.out.println("Unalble to find the account Holder Please try again !!");
			return false;
		}
	}

	public static List<UnitUsage> listUsageByAccount(int accountid) throws SQLException, Exception {
		return unitUsageFactory.listUsageByAccount(accountid);
	}

}
