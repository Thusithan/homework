package EBCSystem;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class UnitUsageFactory {
	public void insertUnitUsage(UnitUsage unitUsage,int accountid) throws SQLException, Exception {
		Connection con = DBConnection.getConnection();
		String query ="INSERT INTO ebc.unit_usage (month, year, unit_count,account_id) VALUES (?, ?, ?,?)";
		PreparedStatement st = con.prepareStatement(query);
		
		st.setString(1, unitUsage.month);
		st.setString(2, unitUsage.year);
		st.setInt(3, unitUsage.unitCount);
		st.setInt(4, accountid);
		st.executeUpdate();
	}
	public List<UnitUsage> listUsageByAccount(int accountid) throws SQLException, Exception {
		Connection con = DBConnection.getConnection();
		String query ="select * from ebc.unit_usage where account_id="+ accountid;
		List<UnitUsage> unitUsages = new ArrayList<UnitUsage>();
		// create the java statement
	      Statement st = con.createStatement();
	      
	      // execute the query, and get a java resultset
	      ResultSet rs = st.executeQuery(query);
	      
	      // iterate through the java resultset
	      while (rs.next())
	      {
	    	UnitUsage uu = new UnitUsage();
	    	uu.id = rs.getInt("idunit_usage");
	    	uu.month = rs.getString("month");
	    	uu.year = rs.getString("year");
	    	uu.unitCount = rs.getInt("unit_count");
	    	unitUsages.add(uu);
	      }
	      st.close();
	      return unitUsages;
	}

}
