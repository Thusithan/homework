package EBCSystem;

import java.sql.*;

public class DBConnection {

	static String url="jdbc:mysql://127.0.0.1:3306/ebc?characterEncoding=utf8&useSSL=false&useUnicode=true";
	static String userName="root";
	static String password="root";
	
	public static Connection getConnection() throws Exception,SQLException {
		Class.forName("com.mysql.jdbc.Driver");
		return DriverManager.getConnection( url, userName, password );
	}	
}
