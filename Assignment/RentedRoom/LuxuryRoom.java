
package Hotel;

public class LuxuryRoom extends RentedRoom {
	private int numberOfDays;

	public LuxuryRoom(int numberOfDays) {
		this.numberOfDays = numberOfDays;
	}

	int getRentalFees() {
		if (this.numberOfDays < 5) {
			return this.numberOfDays * 100;
		} else if (this.numberOfDays >= 5 && this.numberOfDays <= 10) {
			return this.numberOfDays * 100;
		} else {
			return this.numberOfDays * 50;
		}
	}
}
