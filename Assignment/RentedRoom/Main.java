package Hotel;

import java.util.Random;


public class Main {
	
	public static RentedRoom[] rentedRoom = new RentedRoom[6];
	public static void main(String[] args) {
		int i = 0;
		while (i < 6) {
			int randomint = new Random().nextInt(4);
			if (randomint == 1) {
				rentedRoom[i]= new SemiLuxury(i);
				i++;
			} else if (randomint == 2) {
				rentedRoom[i]= new SuperLuxury(i);
				i++;
			} else if (randomint == 3) {
				rentedRoom[i]= new NormalRoom(i);
				i++;
			}
			

		}
		
		for(int j=0;j<6 ;j++) {
			
			if (SemiLuxury.class.isInstance(rentedRoom[j])) {
				SemiLuxury sl = (SemiLuxury)rentedRoom[j];
				System.out.println("SemiLuxury Room is with "+sl.getBeds()+" Beds and Cost is -"+rentedRoom[j].getCost());

			}else if (SuperLuxury.class.isInstance(rentedRoom[j])) {
				SuperLuxury sul = (SuperLuxury)rentedRoom[j];

				System.out.println("SuperLuxury Room is with "+sul.getSquareFeet()+" square Feet and Cost is -"+rentedRoom[j].getCost());
				
			}else if(NormalRoom.class.isInstance(rentedRoom[j])) {
				NormalRoom nl = (NormalRoom)rentedRoom[j];

				System.out.println("NormalRoom Room is rented for  "+nl.getNbDays()+" days and Cost is -"+rentedRoom[j].getCost());
				
			}
		}
	}
}
