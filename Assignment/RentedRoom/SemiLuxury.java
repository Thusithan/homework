
package Hotel;

public class SemiLuxury extends LuxuryRoom{
	private int nbBeds;
	public SemiLuxury(int numberOfDays) {
		super(numberOfDays);
		this.nbBeds =5; 
	}
	

	@Override
	public double  getCost() {
		return this.nbBeds * super.getCost();
	}
	
	public int getBeds() {
		return this.nbBeds;
	}
}
