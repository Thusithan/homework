
package Hotel;

public class SuperLuxury extends LuxuryRoom {

	private double squareFeet;
	public SuperLuxury(int numberOfDays) {
		super(numberOfDays);
		this.squareFeet = 10;
	}
	
	@Override
	public double  getCost() {
		return this.squareFeet * super.getCost();
	}
	public double  getSquareFeet() {
		return this.squareFeet;
	}
}
