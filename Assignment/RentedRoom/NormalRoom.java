
package Hotel;

public class  NormalRoom extends RentedRoom {
	
	private int nbDays;
	public NormalRoom(int nbDays){
		this.nbDays=nbDays;
	}
	
	@Override
	public double  getCost() {
		return this.nbDays * super.getCost();
	}
	
	public double  getNbDays() {
		return this.nbDays ;
	}
}
